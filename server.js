//var express = require('express');
//var path = require('path');
//var favicon = require('serve-favicon');
//var logger = require('morgan');
//var cookieParser = require('cookie-parser');
//var bodyParser = require('body-parser');
//
//var routes = require('./routes/index');
//var users = require('./routes/users');
//
//var app = express();
//
//// view engine setup
//app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'hbs');
//
//// uncomment after placing your favicon in /public
////app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
//app.use(logger('dev'));
//app.use(bodyParser.json());
//app.use(bodyParser.urlencoded({ extended: false }));
//app.use(cookieParser());
//app.use(require('node-sass-middleware')({
//  src: path.join(__dirname, 'public'),
//  dest: path.join(__dirname, 'public'),
//  indentedSyntax: true,
//  sourceMap: true
//}));
//app.use(express.static(path.join(__dirname, 'public')));
//
//app.use('/', routes);
//app.use('/users', users);
//
//// catch 404 and forward to error handler
//app.use(function(req, res, next) {
//  var err = new Error('Not Found');
//  err.status = 404;
//  next(err);
//});
//
//// error handlers
//
//// development error handler
//// will print stacktrace
//if (app.get('env') === 'development') {
//  app.use(function(err, req, res, next) {
//    res.status(err.status || 500);
//    res.render('error', {
//      message: err.message,
//      error: err
//    });
//  });
//}
//
//// production error handler
//// no stacktraces leaked to user
//app.use(function(err, req, res, next) {
//  res.status(err.status || 500);
//  res.render('error', {
//    message: err.message,
//    error: {}
//  });
//});
//
//
//module.exports = app;

var express = require ('express'),
    config = require('config.json')('./conf.json'),
    Twitter = require('twitter');

var app = express();
var IP = process.env.NODE_IP || "localhost";
var PORT = process.env.NODE_PORT || "8081";

app.use(express.static (__dirname + "/public"));

app.get("/", function(req, res){
  res.send("Hello World!");
});

app.get ("/map", function (req, res){
  res.send("Map View!");
});

app.get("/info", function (req, res){
  res.send("Infographic");
});

app.get("/reviewer", function (req, res){
  res.send("Reviewer");
});

app.get("/news", function (req, res){
  res.send("News Watch");
});

// TWITTER CONFIGURATION
var client = new Twitter({
  consumer_key: process.env.TWITTER_CONSUMER_KEY || config.twitter.CONSUMER_KEY,
  consumer_secret: process.env.TWITTER_CONSUMER_SECRET || config.twitter.CONSUMER_SECRET,
  access_token_key: process.env.TWITTER_ACCESS_TOKEN || config.twitter.ACCESS_TOKEN,
  access_token_secret: process.env.TWITTER_ACCESS_SECRET || config.twitter.ACCESS_SECRET
});

// TWITTER CODE
client.stream('statuses/filter', {track: 'javascript'}, function(stream) {
  stream.on('data', function(tweet) {
    console.log(tweet);
  })});

app.listen(PORT, IP);

